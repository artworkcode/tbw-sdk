import TBW from '../TBW';
import { register, login } from './auth';
import { get, me } from './data';
import { setAddress, setCategories, toggleProperty, setDateRequirements, setRestoreHour, setTicketSchema } from './config';

export default class Establishment extends TBW {
  constructor(establishmentName: string = 'playground') {
    super(establishmentName);
  }

  public get = get;

  public register = register;

  public login = login;

  public me = me;

  public setAddress = setAddress;

  public setCategories = setCategories;

  public toggleProperty = toggleProperty;

  public setDateRequirements = setDateRequirements;

  public setRestoreHour = setRestoreHour;

  public setTicketSchema = setTicketSchema;
}