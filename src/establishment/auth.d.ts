import Establishment from './index';
export declare function register(this: Establishment, email: string, password: string, name: string, callback?: (error: Error | undefined, registered: boolean) => void): Promise<boolean> | void;
export declare function login(this: Establishment, email: string, password: string, callback?: (error: Error | undefined, logged: boolean) => void): Promise<boolean> | void;
