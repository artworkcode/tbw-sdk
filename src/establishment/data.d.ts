import { IEstablishmentPublicData, IEstablishmentPrivateData } from '../types';
import Establishment from '.';
export declare function get(this: Establishment, name: string, callback?: (error: Error | undefined, publicData: IEstablishmentPublicData | {}) => void): Promise<IEstablishmentPublicData> | void;
export declare function me(this: Establishment, callback?: (error: Error | undefined, publicData: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void;
