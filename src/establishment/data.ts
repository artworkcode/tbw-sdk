import { IEstablishmentPublicData, IEstablishmentPrivateData } from '../types';
import Establishment from '.';

const noApiError: Error = new Error('You must login first');

export function get(this: Establishment, name: string, callback?: (error: Error | undefined, publicData: IEstablishmentPublicData | {}) => void): Promise<IEstablishmentPublicData> | void {
  if (callback) {
    this.connection.get(`/establishment/${name}`).then((response) => {
      callback(undefined, response.data);
    }).catch((error) => {
      callback(new Error(error.message), {});
    });

    return;
  }

  return new Promise((resolve, reject) => {
    this.connection.get(`/establishment/${name}`).then((response) => {
      resolve(response.data);
    }).catch(reject);
  });
}

export function me(this: Establishment, callback?: (error: Error | undefined, publicData: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void {
  if (callback && this.api) {
    this.api.get('/establishment/me').then((response) => {
      callback(undefined, response.data);
    }).catch((error) => {
      callback(new Error(error.message), {});
    });

    return;
  } else if (callback && !this.api) {
    callback(noApiError, {});

    return;
  }

  return new Promise((resolve, reject) => {
    if (this.api) {
      this.api.get('/establishment/me').then((response) => {
        resolve(response.data);
      }).catch(reject);
    } else {
      reject(noApiError);
    }
  });
}