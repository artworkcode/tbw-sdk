import Establishment from '.';
import { IAddress, IEstablishmentPrivateData, IEstablishmentCategory, property, ticketSchema } from '../types';

const noApiError: Error = new Error('You must login first');

export function setAddress(this: Establishment, Address: IAddress, callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void {
  if (callback && this.api) {
    this.api.put('/establishment/address', Address).then((response) => {
      callback(undefined, response.data);
    }).catch(error => callback(new Error(error.message), {}));

    return;
  } else if (callback && !this.api) {
    callback(noApiError, {});

    return;
  }

  return new Promise((resolve, reject) => {
    if (this.api) {
      this.api.put('/establishment/address', Address).then((response) => {
        resolve(response.data);
      }).catch(error => reject(new Error(error.message)));
    } else {
      reject(noApiError);
    }
  });
};

export function setCategories(this: Establishment, Categories: IEstablishmentCategory[], callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void {
  if (callback && this.api) {
    this.api.put('/establishment/categories', Categories).then((response) => {
      callback(undefined, response.data);
    }).catch(error => callback(new Error(error.message), {}));

    return;
  } else if (callback && !this.api) {
    callback(noApiError, {});

    return;
  }

  return new Promise((resolve, reject) => {
    if (this.api) {
      this.api.put('/establishment/categories', Categories).then((response) => {
        resolve(response.data);
      }).catch(error => reject(new Error(error.message)));
    } else {
      reject(noApiError);
    }
  });
};

export function toggleProperty(this: Establishment, property: property, callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void {
  if (callback && this.api) {
    this.api.put(`/establishment/property/${property}`).then((response) => {
      callback(undefined, response.data);
    }).catch(error => callback(new Error(error.message), {}));

    return;
  } else if (callback && !this.api) {
    callback(noApiError, {});

    return;
  }

  return new Promise((resolve, reject) => {
    if (this.api) {
      this.api.put(`/establishment/property/${property}`).then((response) => {
        resolve(response.data);
      }).catch(error => reject(new Error(error.message)));
    } else {
      reject(noApiError);
    }
  });
};

export function setDateRequirements(this: Establishment, DateRequirements: string[], callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void {
  if (callback && this.api) {
    this.api.put('/establishment/dateRequirements', DateRequirements).then((response) => {
      callback(undefined, response.data);
    }).catch(error => callback(new Error(error.message), {}));

    return;
  } else if (callback && !this.api) {
    callback(noApiError, {});

    return;
  }

  return new Promise((resolve, reject) => {
    if (this.api) {
      this.api.put('/establishment/dateRequirements', DateRequirements).then((response) => {
        resolve(response.data);
      }).catch(error => reject(new Error(error.message)));
    } else {
      reject(noApiError);
    }
  });
};

export function setRestoreHour(this: Establishment, restoreHour: string, callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void {
  if (callback && this.api) {
    this.api.put('/establishment/restoreHour', restoreHour).then((response) => {
      callback(undefined, response.data);
    }).catch(error => callback(new Error(error.message), {}));

    return;
  } else if (callback && !this.api) {
    callback(noApiError, {});

    return;
  }

  return new Promise((resolve, reject) => {
    if (this.api) {
      this.api.put('/establishment/restoreHour', restoreHour).then((response) => {
        resolve(response.data);
      }).catch(error => reject(new Error(error.message)));
    } else {
      reject(noApiError);
    }
  });
};

export function setTicketSchema(this: Establishment, ticketSchema: ticketSchema, callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void {
  if (callback && this.api) {
    this.api.put('/establishment/ticketSchema', ticketSchema).then((response) => {
      callback(undefined, response.data);
    }).catch(error => callback(new Error(error.message), {}));

    return;
  } else if (callback && !this.api) {
    callback(noApiError, {});

    return;
  }

  return new Promise((resolve, reject) => {
    if (this.api) {
      this.api.put('/establishment/ticketSchema', ticketSchema).then((response) => {
        resolve(response.data);
      }).catch(error => reject(new Error(error.message)));
    } else {
      reject(noApiError);
    }
  });
};
