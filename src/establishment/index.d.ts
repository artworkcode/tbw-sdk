import TBW from '../TBW';
import { register, login } from './auth';
import { get, me } from './data';
import { setAddress, setCategories, toggleProperty, setDateRequirements, setRestoreHour, setTicketSchema } from './config';
export default class Establishment extends TBW {
    constructor(establishmentName?: string);
    get: typeof get;
    register: typeof register;
    login: typeof login;
    me: typeof me;
    setAddress: typeof setAddress;
    setCategories: typeof setCategories;
    toggleProperty: typeof toggleProperty;
    setDateRequirements: typeof setDateRequirements;
    setRestoreHour: typeof setRestoreHour;
    setTicketSchema: typeof setTicketSchema;
}
