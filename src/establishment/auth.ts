import Establishment from './index';

export function register(this: Establishment, email: string, password: string, name: string, callback?: (error: Error | undefined, registered: boolean) => void): Promise<boolean> | void {
  if (callback) {
    this.connection.post('/establishment/register', {
      email,
      password,
      name,
    }).then((response) => {
      this.token = response.data.token;
      this.generateSession();
      callback(undefined, true);
    }).catch((error) => {
      callback(new Error(error.message), false);
    });

    return;
  }

  return new Promise((resolve, reject) => {
    this.connection.post('/establishment/register', {
      email,
      password,
      name,
    }).then((response) => {
      this.token = response.data.token;
      this.generateSession();
      resolve(true);
    }).catch((error) => {
      reject(new Error(error.message));
    });
  });
};

export function login(this: Establishment, email: string, password: string, callback?: (error: Error | undefined, logged: boolean) => void): Promise<boolean> | void {
  if (callback) {
    this.connection.post('/establishment/login', {
      email,
      password
    }).then((response) => {
      this.token = response.data.token;
      this.generateSession();
      callback(undefined, true);
    }).catch((error) => {
      callback(new Error(error.message), false);
    });

    return;
  }

  return new Promise((resolve, reject) => {
    this.connection.post('/establishment/login', {
      email,
      password
    }).then((response) => {
      this.token = response.data.token;
      this.generateSession();
      resolve(true);
    }).catch((error) => {
      reject(new Error(error.message));
    });
  });
};
