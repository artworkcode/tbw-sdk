import Establishment from '.';
import { IAddress, IEstablishmentPrivateData, IEstablishmentCategory, property, ticketSchema } from '../types';
export declare function setAddress(this: Establishment, Address: IAddress, callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void;
export declare function setCategories(this: Establishment, Categories: IEstablishmentCategory[], callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void;
export declare function toggleProperty(this: Establishment, property: property, callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void;
export declare function setDateRequirements(this: Establishment, DateRequirements: string[], callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void;
export declare function setRestoreHour(this: Establishment, restoreHour: string, callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void;
export declare function setTicketSchema(this: Establishment, ticketSchema: ticketSchema, callback?: (error: Error | undefined, establishment: IEstablishmentPrivateData | {}) => void): Promise<IEstablishmentPrivateData> | void;
