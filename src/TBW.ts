import axios, { AxiosInstance } from 'axios';

export default class TBW {
  public token: string = '';
  public establishmentName: string;
  public api: AxiosInstance | undefined;
  public errorCallback: (error: Error) => void = () => {};
  public baseURL: string = 'http://localhost:3000/api';

  constructor(establishmentName: string = 'playground') {
    this.establishmentName = establishmentName;
  }

  public connection: AxiosInstance = axios.create({
    baseURL: this.baseURL,
    timeout: 10000,
  });

  public generateSession(): void {
    if (this.token) {
      this.api = axios.create({
        baseURL: this.baseURL,
        timeout: 10000,
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      });
    } else {
      this.errorCallback(new Error(''));
    }
  }

  public onError(callback: (error: Error) => void): void {
    this.errorCallback = callback;
  }
}
