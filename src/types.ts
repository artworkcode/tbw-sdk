export interface IMultimedia {
  name: string;
  url: string;
  mime: string;
  size: number;
  hash: string;
}

export interface ITimestamp {
  creation: string;
  lastModification: string | undefined;
}

export interface IAddress {
  street: string;
  extNumber: string;
  intNumber: string;
  postalCode: string;
  colony: string;
  state: string;
  country: string;
  reference: string;
}

export interface IEstablishmentCategory {
  name: string;
  letter: string;
}

export interface IEstablishmentPublicData {
  name: string;
  address: string;
  Categories: IEstablishmentCategory[];
  Multimedia: IMultimedia[];
  DateRequirements: string[];
}

export interface IEstablishmentPrivateData extends IEstablishmentPublicData {
  email: string;
  // Operators: ;
  rateOperators: boolean;
  allowDates: boolean;
  restoreHour: string;
  ticketSchema: ticketSchema;
  showEstimatedTime: boolean;
  showContent: boolean;
  allowPrinter: boolean;
  TimeStamps: ITimestamp;
}

export type ticketSchema = 'C-N' | 'N';

export type property = 'rateOperators' | 'allowDates' | 'showEstimatedTime' | 'showContent' | 'allowPrinter';