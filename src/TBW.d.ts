import { AxiosInstance } from 'axios';
export default class TBW {
    token: string;
    establishmentName: string;
    api: AxiosInstance | undefined;
    errorCallback: (error: Error) => void;
    baseURL: string;
    constructor(establishmentName?: string);
    connection: AxiosInstance;
    generateSession(): void;
    onError(callback: (error: Error) => void): void;
}
