const path = require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');

module.exports = {
  mode: 'production',
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'tbw-sdk.js',
    publicPath: '/assets/',
    library: 'tbw-sdk',
    libraryTarget: 'commonjs2',
  },
  module: {
    rules: [{
      test: /\.ts$/,
      loader: 'awesome-typescript-loader',
    }],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  devtool: 'source-map',
  target: 'web',
  plugins: [
    new CheckerPlugin(),
  ],
};
